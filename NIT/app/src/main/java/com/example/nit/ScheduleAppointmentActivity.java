package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.AppointmentScheduleDao;
import com.example.nit.dao.DayOffDao;
import com.example.nit.dao.UserDao;
import com.example.nit.dao.WorkTimeDao;
import com.example.nit.entities.AppointmentSchedule;
import com.example.nit.entities.DayOff;
import com.example.nit.entities.User;
import com.example.nit.entities.WorkTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ScheduleAppointmentActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnLongHair;
    Button btnShortHair;
    Button btnShave;
    Button btnShaveAndMassage;
    Button btnSmoothing;
    Button btnSetTime;
    AppointmentSchedule pickDateCutHair;
    User user;

    boolean flag = false;
    int dayOfWeek = 0;
    String hairStyle;
    double price;
    int dayPicked;
    int monthPicked;
    int yearPicked;
    private int usedId;

    WorkTimeDao workTimeDao;
    DayOffDao dayOffDao;
    AppointmentScheduleDao asDao;
    UserDao userDao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_appointment);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        workTimeDao = db.workTimeDao();
        dayOffDao = db.dayOffDao();
        asDao = db.appointmentScheduleDao();
        userDao = db.userDao();

        pickDateCutHair = new AppointmentSchedule();

        btnLongHair = findViewById(R.id.btnLongHair);
        btnLongHair.setOnClickListener(this);
        btnShortHair = findViewById(R.id.btnShortHair);
        btnShortHair.setOnClickListener(this);
        btnShave = findViewById(R.id.btnShave);
        btnShave.setOnClickListener(this);
        btnShaveAndMassage = findViewById(R.id.btnShaveAndMassage);
        btnShaveAndMassage.setOnClickListener(this);
        btnSmoothing = findViewById(R.id.btnSmoothing);
        btnSmoothing.setOnClickListener(this);
        btnSetTime = findViewById(R.id.btnSetTime);
        btnSetTime.setOnClickListener(this);

        SharedPreferences sp = getSharedPreferences("login", MODE_PRIVATE);
        usedId = sp.getInt("userId", 0);

        user = userDao.getUserById(usedId);



    }

    @Override
    public void onClick(View view) {
        pickDateCutHair = new AppointmentSchedule();
        switch (view.getId()) {
            case R.id.btnLongHair:
                hairStyle = "Cắt tóc dài";
                price = 70000;
                showDateTimePicker();
                break;
            case R.id.btnShortHair:
                hairStyle = "Cắt tóc ngắn";
                price = 50000;
                showDateTimePicker();
                break;
            case R.id.btnShave:
                hairStyle = "Cạo râu";
                price = 30000;
                showDateTimePicker();
                break;
            case R.id.btnShaveAndMassage:
                hairStyle = "Cạo râu và mát xa";
                price = 40000;
                showDateTimePicker();
                break;
            case R.id.btnSmoothing:
                hairStyle = "Uốn và nhuộm";
                price = 500000;
                showDateTimePicker();
                break;
            case R.id.btnSetTime:
                if(flag == false) {
                    Context context = getApplicationContext();
                    CharSequence text = "Vui lòng chọn kiểu và ngày hẹn trước";
                    Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    flag = false;
                    showTimePicker();
                }
                break;
        }
    }

    private void showDateTimePicker() {
        Calendar calendar = Calendar.getInstance();
//        dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int currentYear = calendar.get(Calendar.YEAR);
        int currentMonth = calendar.get(Calendar.MONTH) + 1;
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            DatePickerDialog dialog = new DatePickerDialog(this);
            dialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
//                    tv.setText("Selected date: " + day + "/" + (month + 1) + "/" + year);
                    boolean checkDate = true;
                    if(year >= currentYear) {
                        if(month + 1 < currentMonth) {
                            checkDate = false;
                        } else if (month + 1 == currentMonth) {
                            if(day <= currentDay) {
                                checkDate = false;
                            }
                        }
                    } else {
                        checkDate = false;
                    }
                    if(checkDate == true) {
                        DayOff dayOff = dayOffDao.getDayOff(day, month + 1, year);
                        if(dayOff == null) {
                            flag = true;
                            dayPicked = day;
                            monthPicked = month + 1;
                            yearPicked = year;
                            dayOfWeek = checkDayOfWeek(day, month + 1, year);
//                            Context context = getApplicationContext();
//                            CharSequence text = dayOfWeek + "";
//                            Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
//                            toast.show();
                            //System.out.println(pickDateCutHair.getDay() + pickDateCutHair.getMonth() + pickDateCutHair.getYear());
                        } else {
                            Context context = getApplicationContext();
                            CharSequence text = "Ngày hẹn đã chọn là ngày nghỉ";
                            Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                            toast.show();
                        }
                    } else {
                        Context context = getApplicationContext();
                        CharSequence text = "Ngày hẹn đã chọn không khả dụng";
                        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                        toast.show();
                    }



                }
            });
//            dialog.setOnDateSetListener();
            dialog.show();
        }
    }

    private void showTimePicker() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            TimePickerDialog dialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                    WorkTime workTime = workTimeDao.getWorkTime(convertDayOfWeek(dayOfWeek));
                    if(hourOfDay < workTime.getOpenHour() || hourOfDay > workTime.getCloseHour()) {
                        flag = false;
                    } else if(hourOfDay == workTime.getOpenHour()) {
                        if(minute > workTime.getOpenMinute()) {
                            flag = true;
                        } else {
                            flag = false;
                        }
                    } else if (hourOfDay == workTime.getCloseHour()) {
                        if(minute < workTime.getCloseMinute()) {
                            flag = true;
                        } else {
                            flag = false;
                        }
                    } else {
                        flag = true;

                    }
                    if(flag == true) {
                        pickDateCutHair.setTime(hourOfDay + " : " + minute);
                        pickDateCutHair.setHairStyle(hairStyle);
                        pickDateCutHair.setPrice(price + "đ");
                        pickDateCutHair.setDay(dayPicked);
                        pickDateCutHair.setMonth(monthPicked);
                        pickDateCutHair.setYear(yearPicked);
                        Context context = getApplicationContext();
                        CharSequence text = "Đặt lịch thành công";
                        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                        toast.show();
                        pickDateCutHair.setUserId(usedId);
                        asDao.insertAll(pickDateCutHair);

                        Intent intent = new Intent(ScheduleAppointmentActivity.this, UserMenuActivity.class);
                        startActivity(intent);
                    } else {
                        Context context = getApplicationContext();
                        CharSequence text = "Giờ hẹn không hợp lệ";
                        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                        toast.show();
                    }
                }
            }, hour, minute, true);
            dialog.show();

        }
    }

    public String convertDayOfWeek(int day) {
        String dayStr = "";
        switch (day) {
            case 1:
                dayStr = "Sunday";
                break;
            case 2:
                dayStr = "Monday";
                break;
            case 3:
                dayStr = "Tuesday";
                break;
            case 4:
                dayStr = "Wednesday";
                break;
            case 5:
                dayStr = "Thursday";
                break;
            case 6:
                dayStr = "Friday";
                break;
            case 7:
                dayStr = "Saturday";
                break;
        }
        return  dayStr;
    }

    public int checkDayOfWeek(int day, int month, int year) {
        String date = "";
        int dayOfWeek = 0;
        if(day < 10) {
            date+= "0" + day + "/";
        } else {
            date+= day + "/";
        }
        if(month < 10) {
            date+= "0" + month + "/";
        } else {
            date+= month + "/";
        }
        date+= year;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date dateResult = format.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateResult);
            dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        } catch (ParseException e) {
            Context context = getApplicationContext();
            CharSequence text = "Error - invalid date";
            Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
            toast.show();
        }
        return dayOfWeek;
    }
}