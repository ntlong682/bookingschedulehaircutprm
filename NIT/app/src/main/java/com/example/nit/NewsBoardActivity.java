package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.NewDao;
import com.example.nit.entities.AppointmentSchedule;
import com.example.nit.entities.News;

import java.util.ArrayList;
import java.util.List;

public class NewsBoardActivity extends AppCompatActivity {

    ListView listNews;
    ArrayAdapter<News> adapter;
    NewDao newDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_board);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        newDao = db.newDao();

        listNews = findViewById(R.id.lstNewBoard);
        loadData();

    }

    public void loadData(){
        List<News> listNewFromDB = newDao.getAllNews();
        List<News> listNewPrint = new ArrayList<>();
//        for(News news : listNewFromDB) {
//            News news1 = new News(news.getId(), news.getContent());
//            listNewPrint.add(news1);
//        }
        int sizeListNews = listNewFromDB.size();
        for(int i = sizeListNews - 1; i >= 0; i--) {
            News news1 = new News(listNewFromDB.get(i).getId(), listNewFromDB.get(i).getContent());
            listNewPrint.add(news1);
        }
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listNewPrint);
        listNews.setAdapter(adapter);
    }

}