package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.NewDao;
import com.example.nit.entities.News;

public class AddNewsActivity extends AppCompatActivity {

    EditText edtAddNews;
    Button btnAdd;
    NewDao newDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        newDao = db.newDao();

        edtAddNews = findViewById(R.id.edtAddNews);
        btnAdd = findViewById(R.id.btnAddNewsBoard);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newText = edtAddNews.getText().toString();
                CharSequence charContext;
                if(newText == null || newText.isEmpty()) {
                    charContext = "Thông báo không được để trống";
                } else {
                    News news = new News(newText);
                    newDao.insertAll(news);
                    charContext = "Thêm thông báo mới thành công";

                }
                Context context = getApplicationContext();
                Toast toast = Toast.makeText(context, charContext, Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }
}