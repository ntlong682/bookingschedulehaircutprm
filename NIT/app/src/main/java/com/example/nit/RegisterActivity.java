package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.UserDao;
import com.example.nit.entities.User;

public class RegisterActivity extends AppCompatActivity {

    EditText edtUserName;
    EditText edtPassword;
    EditText edtRePassword;
    EditText edtFullName;
    EditText edtPhoneNumber;
    Button btnRegister;
    TextView txtRegisterMessage;
    UserDao userDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        userDao = db.userDao();

        edtUserName = findViewById(R.id.edtUsernameRegister);
        edtPassword = findViewById(R.id.edtPasswordRegister);
        edtRePassword = findViewById(R.id.edtRePasswordRegister);
        edtFullName = findViewById(R.id.edtFullnameRegister);
        edtPhoneNumber = findViewById(R.id.edtPhoneRegister);
        btnRegister = findViewById(R.id.btnRegisterConfirm);
        txtRegisterMessage = findViewById(R.id.txtRegisterMessage);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = edtUserName.getText().toString();
                if(userName.trim() == null || userName.trim().isEmpty()) {
                    txtRegisterMessage.setText("Username không được để trống");
                } else {
                    try {
                        if(userDao.checkUserNameExist(userName) > 0) {
                            txtRegisterMessage.setText("Username đã tồn tại");
                        } else {
                            String password = edtPassword.getText().toString();
                            if(password.trim() == null || password.trim().isEmpty()) {
                                txtRegisterMessage.setText("Password không được để trống");
                            } else {
                                String rePassword = edtRePassword.getText().toString();
                                if(!password.equals(rePassword)) {
                                    txtRegisterMessage.setText("Re-password hiện không trùng với password");
                                } else {
                                    boolean flag = true;
                                    String fullName = edtFullName.getText().toString();
                                    String phoneNumber = edtPhoneNumber.getText().toString();
                                    if(fullName.trim() == null || fullName.trim().isEmpty()) {
                                       txtRegisterMessage.setText("Full name không được để trống");
                                        flag = false;
                                    }
                                    if(phoneNumber.trim() == null || phoneNumber.trim().isEmpty() || phoneNumber.length() > 11) {
                                        txtRegisterMessage.setText("Phone number invalid");
                                        flag = false;
                                    }
                                    if(flag == true) {
                                        User newUser = new User(userName, password, fullName, phoneNumber, 1);
                                        userDao.insertAll(newUser);
                                        txtRegisterMessage.setText("Đăng kí thành công");
                                        edtUserName.setText("");
                                        edtPassword.setText("");
                                        edtRePassword.setText("");
                                        edtFullName.setText("");
                                        edtPhoneNumber.setText("");
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        txtRegisterMessage.setText("Đăng kí thất bại");
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
    }
}