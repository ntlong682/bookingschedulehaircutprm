package com.example.nit;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.AppointmentScheduleDao;
import com.example.nit.entities.AppointmentSchedule;

import java.util.ArrayList;
import java.util.List;

public class BookedHistoryActivity extends AppCompatActivity {

    AppointmentScheduleDao appointmentScheduleDao;
    private int usedId;
    ListView lstBookedHistory;
    List<AppointmentSchedule> listBookedPrint;
    ArrayAdapter<AppointmentSchedule> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booked_history);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        appointmentScheduleDao = db.appointmentScheduleDao();

        SharedPreferences sp = getSharedPreferences("login", MODE_PRIVATE);
        usedId = sp.getInt("userId", 0);

        lstBookedHistory = findViewById(R.id.lstBookedHistory);
        loadData();

        lstBookedHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showArletDialog(i);
            }
        });
    }

    public void showArletDialog(int postition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Confirm dialog")
                .setMessage("Bạn có muốn hủy lịch hẹn không")
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AppointmentSchedule deleteAp = listBookedPrint.get(postition);
//                            Toast toast = Toast.makeText(getApplicationContext(), deleteAp.toString(), Toast.LENGTH_LONG);
//                            toast.show();
                        listBookedPrint.remove(postition);
                        appointmentScheduleDao.delete(deleteAp);
                        adapter.notifyDataSetChanged();
                        Toast toast = Toast.makeText(getApplicationContext(), "Hủy lịch hẹn thành công", Toast.LENGTH_LONG);
                        toast.show();
                    }
                });
        builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        Dialog dialog = builder.create();
        dialog.show();
    }

    public void loadData(){
        List<AppointmentSchedule> listBooked = appointmentScheduleDao.getAppointmentScheduleByUserId(usedId);
        listBookedPrint = new ArrayList<>();
        for(AppointmentSchedule as : listBooked) {
            AppointmentSchedule newAs = new AppointmentSchedule(as.getId(), as.getUserId(), as.getDay(),
                    as.getMonth(), as.getYear(), as.getTime(), as.getHairStyle(), as.getPrice());
            listBookedPrint.add(newAs);
        }
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listBookedPrint);
        lstBookedHistory.setAdapter(adapter);
    }
}