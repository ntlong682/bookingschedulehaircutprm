package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.UserDao;
import com.example.nit.entities.User;

public class ManagementAdminAccount extends AppCompatActivity {

    EditText edtAdminName;
    EditText edtAdminUsername;
    EditText edtAdminPhone;
    EditText edtAdminOldPassword;
    EditText edtAdminNewPassword;
    EditText edtAdminRePassword;

    Button btnUpdateAdminAccount;
    Button btnUpdateAdminPassword;

    UserDao userDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management_admin_account);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        userDao = db.userDao();

        edtAdminName = findViewById(R.id.edtAdminName);
        edtAdminUsername = findViewById(R.id.edtAdminUsername);
        edtAdminPhone = findViewById(R.id.edtAdminPhoneNumber);
        edtAdminOldPassword = findViewById(R.id.edtAdminOldPassword);
        edtAdminNewPassword = findViewById(R.id.edtAdminNewPassword);
        edtAdminRePassword = findViewById(R.id.edtAdminRePassword);

        btnUpdateAdminAccount = findViewById(R.id.btnUpdateAdminAccount);
        btnUpdateAdminPassword = findViewById(R.id.btnUpdateAdminPassword);

        SharedPreferences sp = getSharedPreferences("login", MODE_PRIVATE);
        int userId = sp.getInt("userId", 0);

        User user = userDao.getUserById(userId);
        edtAdminName.setText(user.getFullName());
        edtAdminUsername.setText(user.getUserName());
        edtAdminPhone.setText(user.getPhoneNumber());

        btnUpdateAdminAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String adminName = edtAdminName.getText().toString();
                String adminUsername = edtAdminUsername.getText().toString();
                String adminPhone = edtAdminPhone.getText().toString();
                if(adminName.isEmpty() || adminUsername.isEmpty() || adminPhone.isEmpty() || adminPhone.length() > 11) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Thông tin không được để trống", Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    user.setFullName(adminName);
                    user.setUserName(adminUsername);
                    user.setPhoneNumber(adminPhone);
                    userDao.update(user);
                    Toast toast = Toast.makeText(getApplicationContext(), "Cập nhật thành công", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        btnUpdateAdminPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String oldPassword = edtAdminOldPassword.getText().toString();
                String newPassword = edtAdminNewPassword.getText().toString();
                String rePassword = edtAdminRePassword.getText().toString();
                if(oldPassword.isEmpty() || newPassword.isEmpty() || rePassword.isEmpty()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Password không được để trống", Toast.LENGTH_LONG);
                    toast.show();
                } else if(!oldPassword.equals(user.getPassword())) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Password sai", Toast.LENGTH_LONG);
                    toast.show();
                } else if(!newPassword.equals(rePassword)) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Password nhập lại không đúng", Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    user.setPassword(newPassword);
                    userDao.update(user);
                    edtAdminOldPassword.setText("");
                    edtAdminNewPassword.setText("");
                    edtAdminRePassword.setText("");
                    Toast toast = Toast.makeText(getApplicationContext(), "Cập nhật thành công", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

    }
}