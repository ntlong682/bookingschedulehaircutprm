package com.example.nit.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.nit.entities.AppointmentSchedule;
import com.example.nit.entities.DayOff;
import com.example.nit.entities.News;
import com.example.nit.entities.User;
import com.example.nit.entities.WorkTime;

@Database(entities = {User.class, AppointmentSchedule.class, DayOff.class, WorkTime.class, News.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public  abstract  UserDao userDao();
    public  abstract  AppointmentScheduleDao appointmentScheduleDao();
    public  abstract  DayOffDao dayOffDao();
    public  abstract  WorkTimeDao workTimeDao();
    public  abstract  NewDao newDao();


}
