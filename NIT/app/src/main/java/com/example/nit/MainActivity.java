package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.UserDao;
import com.example.nit.dao.WorkTimeDao;
import com.example.nit.entities.User;
import com.example.nit.entities.WorkTime;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    EditText edtUsername;
    EditText edtPassword;
    TextView txtLoginMessage;
    Button btnLogin;
    Button btnRegister;
    MainActivity mainActivity;
    UserDao userDao;
    WorkTimeDao workTimeDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        userDao = db.userDao();
        workTimeDao = db.workTimeDao();
//        User admin = new User("admin", "admin", "Admintrastor", "0999999999", 0);
//        userDao.insertAll(admin);
//        WorkTime monday = new WorkTime("Monday", 7, 30, 17, 30);
//        workTimeDao.insertAll(monday);
//        WorkTime tuesday = new WorkTime("Tuesday", 7, 30, 17, 30);
//        workTimeDao.insertAll(tuesday);
//        WorkTime wednesday = new WorkTime("Wednesday", 7, 30, 17, 30);
//        workTimeDao.insertAll(wednesday);
//        WorkTime thursday = new WorkTime("Thursday", 7, 30, 17, 30);
//        workTimeDao.insertAll(thursday);
//        WorkTime friday = new WorkTime("Friday", 7, 30, 17, 30);
//        workTimeDao.insertAll(friday);
//        WorkTime saturday = new WorkTime("Saturday", 7, 30, 17, 30);
//        workTimeDao.insertAll(saturday);
//        WorkTime sunday = new WorkTime("Sunday", 7, 30, 17, 30);
//        workTimeDao.insertAll(sunday);

        edtUsername = findViewById(R.id.edtUserName);
        edtPassword = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);
        txtLoginMessage = findViewById(R.id.txtLoginMessage);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == btnLogin.getId()) {
            String userName = edtUsername.getText().toString();
            String password = edtPassword.getText().toString();
            User loginUser = userDao.getLoginUser(userName, password);
            //System.out.println(loginUser.toString());
//            System.out.println(userName + " " + password);
            if(loginUser != null) {
                SharedPreferences sp = getSharedPreferences("login", MODE_PRIVATE);
                sp.edit().putInt("userId", loginUser.getId()).apply();
                if(loginUser.getRole() == 0) {
                    Intent intent = new Intent(MainActivity.this, AdminMenuActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this, UserMenuActivity.class);
                    startActivity(intent);
                }
            } else {
                txtLoginMessage.setText("Login failed");
            }
        } else if(view.getId() == btnRegister.getId()) {
            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        startActivity(intent);
    }


}