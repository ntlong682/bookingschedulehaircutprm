package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.WorkTimeDao;
import com.example.nit.entities.WorkTime;

import java.util.Calendar;
import java.util.List;

public class EditDateActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtOpenTime;
    TextView txtCloseTime;
    Button btnEditOpenTime;
    Button btnEditCloseTime;
    ArrayAdapter<WorkTime> adapter;
    Spinner spinnerListDay;

    WorkTimeDao workTimeDao;
    WorkTime dayEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_date);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        workTimeDao = db.workTimeDao();

        txtOpenTime = findViewById(R.id.txtOpenTime);
        txtCloseTime = findViewById(R.id.txtCloseTime);

        spinnerListDay = findViewById(R.id.spinnerListDay);

        btnEditOpenTime = findViewById(R.id.btnEditOpenTime);
        btnEditOpenTime.setOnClickListener(this);
        btnEditCloseTime = findViewById(R.id.btnEditCloseTime);
        btnEditCloseTime.setOnClickListener(this);

        List<WorkTime> listWorkDay = workTimeDao.getAllWorkTime();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, listWorkDay);
        spinnerListDay.setAdapter(adapter);

        spinnerListDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Adapter adapterSelected = adapterView.getAdapter();
                dayEdit = (WorkTime) adapterSelected.getItem(i);
                txtOpenTime.setText(dayEdit.getOpenHour() + ":" + dayEdit.getOpenMinute());
                txtCloseTime.setText(dayEdit.getCloseHour() + ":" + dayEdit.getCloseMinute());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                if(dayEdit == null) {
                    spinnerListDay.setSelection(0);
                }
            }
        });

//        spinnerListDay.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Adapter adapterSelected = adapterView.getAdapter();
//                dayEdit = (WorkTime) adapterSelected.getItem(i);
//                txtOpenTime.setText(dayEdit.getOpenHour() + ":" + dayEdit.getOpenMinute());
//                txtCloseTime.setText(dayEdit.getCloseHour() + ":" + dayEdit.getCloseMinute());
//
//            }
//        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnEditOpenTime:
                    showTimePicker(false);
                break;
            case R.id.btnEditCloseTime:
                    showTimePicker(true);
                break;

        }
    }

    private void showTimePicker(boolean checkClose) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            TimePickerDialog dialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                    if(checkClose == true) {
                        boolean flag = true;
                        if(hourOfDay < dayEdit.getOpenHour()) {
                            flag = false;
                        } else if(hourOfDay == dayEdit.getOpenHour() && minute <= dayEdit.getOpenMinute()){
                            flag = false;
                        } else {
                            dayEdit.setCloseHour(hourOfDay);
                            dayEdit.setCloseMinute(minute);
                            txtCloseTime.setText(hourOfDay + ":" + minute);
                        }
                        if(flag == false) {
                            Context context = getApplicationContext();
                            CharSequence text = "Giờ đóng cửa phải muộn hơn giờ mở";
                            Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                            toast.show();
                        }
                    } else {
                        dayEdit.setOpenHour(hourOfDay);
                        dayEdit.setOpenMinute(minute);
                        txtOpenTime.setText(hourOfDay + ":" + minute);
                    }
                    workTimeDao.updateWorkTime(dayEdit);
                }
            }, hour, minute, true);
            dialog.show();

        }
    }
}