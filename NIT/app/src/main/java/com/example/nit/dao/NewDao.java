package com.example.nit.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.nit.entities.News;

import java.util.List;

@Dao
public interface NewDao {

    @Query("SELECT * FROM news")
    List<News> getAllNews();

    @Insert
    void insertAll(News... news);
}
