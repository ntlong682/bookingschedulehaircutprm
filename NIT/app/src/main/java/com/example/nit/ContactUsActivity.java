package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.UserDao;
import com.example.nit.entities.User;

public class ContactUsActivity extends AppCompatActivity {

    User adminAcc;
    UserDao userDao;

    EditText edtAdminPhone;
    EditText edtAdminName;
    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        userDao = db.userDao();

        adminAcc = userDao.getLoginUser("admin", "admin");

        edtAdminPhone = findViewById(R.id.edtAdminContactPhone);
        edtAdminName = findViewById(R.id.edtAdminContactName);
        edtAdminName.setText(adminAcc.getFullName());
        edtAdminName.setEnabled(false);
        edtAdminPhone.setText(adminAcc.getPhoneNumber());
        edtAdminPhone.setEnabled(false);

        btnBack = findViewById(R.id.btnBackFromContact);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContactUsActivity.this, UserMenuActivity.class);
                startActivity(intent);
            }
        });


    }
}