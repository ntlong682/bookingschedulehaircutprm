package com.example.nit.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class AppointmentSchedule implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "userId")
    private int userId;
    @ColumnInfo(name = "day")
    private int day;
    @ColumnInfo(name = "month")
    private int month;
    @ColumnInfo(name = "year")
    private int year;
    @ColumnInfo(name = "time")
    private String time;
    @ColumnInfo(name = "hair_style")
    private String hairStyle;
    @ColumnInfo(name = "price")
    private String price;

    public AppointmentSchedule() {
    }

    public AppointmentSchedule(int id, int userId, int day, int month, int year, String time, String hairStyle, String price) {
        this.id = id;
        this.userId = userId;
        this.day = day;
        this.month = month;
        this.year = year;
        this.time = time;
        this.hairStyle = hairStyle;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getHairStyle() {
        return hairStyle;
    }

    public void setHairStyle(String hairStyle) {
        this.hairStyle = hairStyle;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Ngày: " + day + " tháng: " + month + " năm: " + year + " thời gian: " + time  +
                " kiểu: " + hairStyle +
                " giá " + price;
    }
}
