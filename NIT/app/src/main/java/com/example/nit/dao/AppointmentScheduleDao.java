package com.example.nit.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.nit.entities.AppointmentSchedule;

import java.util.List;

@Dao
public interface AppointmentScheduleDao {

    @Query("SELECT * FROM appointmentschedule WHERE userId LIKE :userId")
    List<AppointmentSchedule> getAppointmentScheduleByUserId(int userId);

    @Query("SELECT * FROM appointmentschedule")
    List<AppointmentSchedule> getAll();

    @Insert
    void insertAll(AppointmentSchedule... appointmentSchedules);

    @Delete
    void delete(AppointmentSchedule appointmentSchedule);
}
