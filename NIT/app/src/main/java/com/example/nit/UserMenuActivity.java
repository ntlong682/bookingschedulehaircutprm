package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class UserMenuActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnScheduleAppointment;
    Button btnShowAppointment;
    Button btnNewsBoard;
    Button btnProfile;
    Button btnContact;
    Button btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_menu);

        btnScheduleAppointment = findViewById(R.id.btnScheduleAppointment);
        btnScheduleAppointment.setOnClickListener(this);
        btnShowAppointment = findViewById(R.id.btnShowBookedAppointment);
        btnShowAppointment.setOnClickListener(this);
        btnNewsBoard = findViewById(R.id.btnNewsBoard);
        btnNewsBoard.setOnClickListener(this);
        btnProfile = findViewById(R.id.btnProfile);
        btnProfile.setOnClickListener(this);
        btnContact = findViewById(R.id.btnContactUs);
        btnContact.setOnClickListener(this);
        btnLogout = findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnScheduleAppointment:
                intent = new Intent(UserMenuActivity.this, ScheduleAppointmentActivity.class);
                startActivity(intent);
                break;
            case R.id.btnShowBookedAppointment:
                intent = new Intent(UserMenuActivity.this, BookedHistoryActivity.class);
                startActivity(intent);
                break;
            case R.id.btnNewsBoard:
                intent = new Intent(UserMenuActivity.this, NewsBoardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnProfile:
                intent = new Intent(UserMenuActivity.this, UserProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.btnContactUs:
                intent = new Intent(UserMenuActivity.this, ContactUsActivity.class);
                startActivity(intent);
                break;
            case R.id.btnLogout:
                intent = new Intent(UserMenuActivity.this, MainActivity.class);
                startActivity(intent);
                break;
        }

    }
}