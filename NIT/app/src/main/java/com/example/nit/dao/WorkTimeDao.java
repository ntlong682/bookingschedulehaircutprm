package com.example.nit.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.nit.entities.WorkTime;

import java.util.List;

@Dao
public interface WorkTimeDao {

    @Query("SELECT * FROM worktime WHERE name LIKE :name")
    WorkTime getWorkTime(String name);

    @Query("SELECT * FROM worktime")
    List<WorkTime> getAllWorkTime();

    @Insert
    void insertAll(WorkTime... workTimes);

    @Update
    void updateWorkTime(WorkTime workTimeDao);
}
