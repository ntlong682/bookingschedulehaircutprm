package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.DayOffDao;
import com.example.nit.entities.DayOff;

import java.util.Calendar;

public class AdminMenuActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnEditDate;
    Button btnEditWorkTime;
    Button btnAddNews;
    Button btnProfile;
    Button btnBookedList;
    Button btnLogOut;

    DayOffDao dayOffDao;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_menu);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        dayOffDao = db.dayOffDao();

        btnEditDate = findViewById(R.id.btnEditDate);
        btnEditDate.setOnClickListener(this);
        btnEditWorkTime = findViewById(R.id.btnEditWorkTime);
        btnEditWorkTime.setOnClickListener(this);
        btnAddNews = findViewById(R.id.btnAddNews);
        btnAddNews.setOnClickListener(this);
        btnProfile = findViewById(R.id.btnUpdateAdminAcc);
        btnProfile.setOnClickListener(this);
        btnBookedList = findViewById(R.id.btnBookedList);
        btnBookedList.setOnClickListener(this);
        btnLogOut = findViewById(R.id.btnLogoutAdmin);
        btnLogOut.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnEditDate:
                showDateTimePicker();
                break;
            case R.id.btnEditWorkTime:
                intent = new Intent(AdminMenuActivity.this, EditDateActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAddNews:
                intent = new Intent(AdminMenuActivity.this, AddNewsActivity.class);
                startActivity(intent);
                break;
            case R.id.btnUpdateAdminAcc:
                intent = new Intent(AdminMenuActivity.this, ManagementAdminAccount.class);
                startActivity(intent);
                break;
            case R.id.btnBookedList:
                intent = new Intent(AdminMenuActivity.this, ListBookedFromUSer.class);
                startActivity(intent);

                break;
            case R.id.btnLogoutAdmin:
                intent = new Intent(AdminMenuActivity.this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void showDateTimePicker() {
        Calendar calendar = Calendar.getInstance();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            DatePickerDialog dialog = new DatePickerDialog(this);
            dialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
//                    tv.setText("Selected date: " + day + "/" + (month + 1) + "/" + year);
                    DayOff dayOff = dayOffDao.getDayOff(day, month + 1, year);
                    if(dayOff != null) {
                        dayOffDao.delete(dayOff);
                        Context context = getApplicationContext();
                        CharSequence text = "Xóa ngày nghỉ thành công";
                        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                        toast.show();
                    } else {
                        dayOff = new DayOff(day, month + 1, year);
                        dayOffDao.insertDay(dayOff);
                        Context context = getApplicationContext();
                        CharSequence text = "Định ngày nghỉ thành công";
                        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                        toast.show();
                    }
                }
            });
//            dialog.setOnDateSetListener();
            dialog.show();
        }
    }
}