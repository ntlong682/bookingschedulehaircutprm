package com.example.nit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nit.dao.AppDatabase;
import com.example.nit.dao.UserDao;
import com.example.nit.entities.User;

public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edtFullName;
    EditText edtUsername;
    EditText edtOldPassword;
    EditText edtNewPassword;
    EditText edtReNewPassword;
    Button btnUpdateProfile;
    Button btnUpdatePassword;

    UserDao userDao;
    User user;

    int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "HairCutStorage").allowMainThreadQueries().build();
        userDao = db.userDao();

        SharedPreferences sp = getSharedPreferences("login", MODE_PRIVATE);
        userId = sp.getInt("userId", 0);

        user = userDao.getUserById(userId);

        edtFullName = findViewById(R.id.edtFullnameProfile);
        edtUsername = findViewById(R.id.edtUsernameProfile);
        edtOldPassword = findViewById(R.id.edtOldPasswordProfile);
        edtNewPassword = findViewById(R.id.edtNewPassword);
        edtReNewPassword = findViewById(R.id.edtReNewPassword);

        btnUpdateProfile = findViewById(R.id.btnUpdateProfile);
        btnUpdateProfile.setOnClickListener(this);
        btnUpdatePassword = findViewById(R.id.btnUpdatePassword);
        btnUpdatePassword.setOnClickListener(this);

        edtFullName.setText(user.getFullName());
        edtUsername.setText(user.getUserName());


    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnUpdateProfile) {
            String newFullName = edtFullName.getText().toString();
            String newUsername = edtUsername.getText().toString();
            if(newFullName == null || newFullName.isEmpty() || newUsername == null || newUsername.isEmpty()) {
                Context context = getApplicationContext();
                CharSequence text = "Họ tên hoặc username không hợp lệ";
                Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                toast.show();
            } else {
                user.setFullName(newFullName);
                user.setUserName(newUsername);
                userDao.update(user);
                Context context = getApplicationContext();
                CharSequence text = "Cập nhật thành công";
                Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                toast.show();
            }
        }
        if(view.getId() == R.id.btnUpdatePassword) {
            String oldPassword = edtOldPassword.getText().toString();
            String newPassword = edtNewPassword.getText().toString();
            String reNewPassword = edtReNewPassword.getText().toString();
            if(oldPassword == null || oldPassword.isEmpty() || newPassword  == null || newPassword.isEmpty()
            || reNewPassword == null || reNewPassword.isEmpty()) {
                Context context = getApplicationContext();
                CharSequence text = "Password không hợp lệ, vui lòng không để trống";
                Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                toast.show();
            } else {
                String oldpass = user.getPassword();
                if(!oldPassword.equals(oldpass)) {
                    Context context = getApplicationContext();
                    CharSequence text = "Password sai, vui lòng nhập lại";
                    Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                    toast.show();
                } else if(!newPassword.equals(reNewPassword)){
                    Context context = getApplicationContext();
                    CharSequence text = "Password và password nhập lại phải giống nhau";
                    Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    user.setPassword(newPassword);
                    userDao.update(user);
                    Context context = getApplicationContext();
                    CharSequence text = "Cập nhật thành công";
                    edtOldPassword.setText("");
                    edtNewPassword.setText("");
                    edtReNewPassword.setText("");
                    Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        }
    }
}