package com.example.nit.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.nit.entities.DayOff;
@Dao
public interface DayOffDao {

    @Query("SELECT * FROM dayoff WHERE day LIKE :day AND month LIKE :month AND year LIKE :year")
    DayOff getDayOff(int day, int month, int year);

    @Insert
    void insertDay(DayOff... dayOffs);

    @Delete
    void delete(DayOff dayOff);
}
