package com.example.nit.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.nit.entities.User;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user WHERE user_name LIKE :first AND password LIKE :last")
    User getLoginUser(String first, String last);

    @Query("SELECT * FROM user WHERE id LIKE :id")
    User getUserById(int id);

    @Query("SELECT * FROM user WHERE id LIKE :userName")
    User getUserByUsername(String userName);

    @Query("SELECT COUNT (*) FROM user WHERE user_name LIKE :username")
    int checkUserNameExist(String username);

    @Insert
    void insertAll(User... users);

    @Delete
    void delete(User user);

    @Update
    void update(User user);
}
